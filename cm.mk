# Boot animation
TARGET_SCREEN_HEIGHT := 1024
TARGET_SCREEN_WIDTH := 768

# Inherit some common CyanogenMod stuff
$(call inherit-product, vendor/cm/config/common_full_tablet_wifionly.mk)

# Inherit device configuration
$(call inherit-product, device/rockchip/u8/full_u8.mk)

# Device identifier. This must come after all inclusions
PRODUCT_NAME := cm_u8
# Set product name
PRODUCT_BUILD_PROP_OVERRIDES += \
    PRODUCT_NAME=pipo_u8
